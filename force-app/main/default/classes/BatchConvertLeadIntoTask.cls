public class BatchConvertLeadIntoTask implements Database.Batchable<SObject> 
{
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator('SELECT Id, lastName,  ConvertedAccountId, ConvertedContactId, OwnerId FROM Lead WHERE ConvertedDate > LAST_N_YEARS : 1'); 
    }
    
    public void execute(Database.BatchableContext bc, List<Lead> lstleads)
    {	
        List<Task> lstTask = [SELECT WhoId, Subject FROM Task WHERE Subject = 'Please reach out to the Contact to understand next steps'  ];
        Set<Id> setWhoIdTask = new Set<Id>();
         List<Task> lstTasks = new List<Task>();
        for(Task objTask : lstTask)
        {
           setWhoIdTask.add(objTask.WhoId);
        }
        
        for(Lead objLeads : lstleads )
        {
            if(!setWhoIdTask.contains(objLeads.ConvertedContactId))
            {
                Task objtask = new Task();
                objtask.WhoId = objLeads.ConvertedContactId;
                objtask.WhatId = objLeads.ConvertedAccountId;
                objtask.OwnerId = objleads.OwnerId;
                
                objtask.Subject = 'Please reach out to the Contact to understand next steps';
                objtask.Priority = 'High';
                objtask.Status = 'Not Started';
                objtask.Type = 'Phone';
                lstTasks.add(objtask);
            }
        }
        if(!lstTasks.isEmpty())
        {
            insert lstTasks;
        }
      
    }
    
    public void finish(Database.BatchableContext bc)
    {
        System.debug('Done');
    }
}



/*
        //Set<Id> setTaskIds = new Set<Task>();
        Map<Id,String> mapTaskIdsSub = new Map<Id, String>();
        List<Task> lstTaskIds = [SELECT WhoId, Subject FROM Task];
        for(Task objtaskIds : lstTaskIds )
        {
            if(!mapTaskIdsSub.containsKey(objtaskIds.WhoId))
            {
                mapTaskIdsSub.put(objtaskIds.WhoId,objtaskIds.Subject);
            }
            else
            {
                boolean boolcheckSub = mapTaskIdsSub.get(objtaskIds.WhoId).contains(objtaskIds.Subject);
                if(!boolcheckSub)
                {
                    String strLeadSubject = mapTaskIdsSub.get(objtaskIds.WhoId) +','+ objtaskIds.Subject;
                    mapTaskIdsSub.put(objtaskIds.WhoId, strLeadSubject);
                }
            }
        }
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<');
        System.debug(maptaskIdsSub);
        
        for(Lead objLeads : lstleads)
        {
            if(mapTaskIdsSub.containsKey(objLeads.ConvertedContactId))
            {
                String strSubject = mapTaskIdsSub.get(objLeads.ConvertedContactId);
                System.debug('<<<<<<<<>>>>>>>>>>MMMMMAAAAAAPPPPPP<<<<<<>>>>>>>');
                
                System.debug(mapTaskIdsSub.get(objLeads.ConvertedContactId));
                boolean bool = mapTaskIdsSub.get(objLeads.ConvertedContactId).contains(strSubject);
                System.debug('>>>>>>>>BOOOOOOL<<<<<<<<<<<<<<<<');
              	System.debug(bool);
                if(!bool)             
                {
                    Task objtask = new Task();
                    objtask.WhoId = objLeads.ConvertedContactId;
                    objtask.WhatId = objLeads.ConvertedAccountId;
                    objtask.OwnerId = objleads.OwnerId;
                    
                    objtask.Subject = 'Please reach out to the Contact to understand next steps';
                    objtask.Priority = 'High';
                    objtask.Status = 'Not Started';
                    objtask.Type = 'Phone';
                    lstTasks.add(objtask);
                }
            }
            else
            {
                Task objtask = new Task();
                objtask.WhoId = objLeads.ConvertedContactId;
                objtask.WhatId = objLeads.ConvertedAccountId;
                objtask.OwnerId = objleads.OwnerId;
                
                objtask.Subject = 'Please reach out to the Contact to understand next steps';
                objtask.Priority = 'High';
                objtask.Status = 'Not Started';
                objtask.Type = 'Phone';
                lstTasks.add(objtask);
            }
            
        } 
*/