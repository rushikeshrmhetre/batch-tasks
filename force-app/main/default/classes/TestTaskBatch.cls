@isTest
public class TestTaskBatch 
{
    @isTest
    static void setupMethod()
    {
        Lead objLead = new Lead();
        objLead.FirstName = 'testfirst1';
        objLead.LastName = 'testlast2';
        objLead.Company = 'Silverline';
        insert objLead;
        
        Database.LeadConvert objlc = new Database.LeadConvert();
        
        objlc.setLeadId(objLead.Id);
        LeadStatus convertstatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        
        objlc.setConvertedStatus(convertstatus.MasterLabel);
        Database.LeadConvertResult leadconv = Database.convertLead(objlc);
        
        System.assert(leadconv.isSuccess());
        
        Lead objLead2 = new Lead();
        objLead2.FirstName = 'testfirst2';
        objLead2.LastName = 'testlast2';
        objLead2.Company = 'Google';
        insert objLead2;
        
        Database.LeadConvert objlc2 = new Database.LeadConvert();
        objlc2.setLeadId(objLead2.Id);
        
        LeadStatus convertstatus1 = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = true LIMIT 1];
        objlc2.setConvertedStatus(convertstatus1.MasterLabel);
        
        Database.LeadConvertResult leadconv1 = Database.convertLead(objlc2);
        System.assert(leadconv1.isSuccess());
        
        Lead objNewLead = [SELECT ConvertedContactId,Id,OwnerId from Lead WHERE Id = :objLead2.Id];
        Task objTask = new Task(WhoId = objNewLead.ConvertedContactId, OwnerId = objNewLead.OwnerId, Subject = 'Please reach out to the Contact to understand next steps');
        insert objTask;
        
        Test.startTest();
        BatchConvertLeadIntoTask leadCreate = new BatchConvertLeadIntoTask();
        Database.executeBatch(leadCreate);
        Test.stopTest();
        
        Lead objleadtest = [SELECT Id, ConvertedContactId from Lead WHERE Id = :objLead2.Id];
        Task objtasktest = [SELECT WhoId from Task WHERE WhoId = :objleadtest.ConvertedContactId];
        System.assertEquals(objleadtest.ConvertedContactId, objtasktest.WhoId);
        
    }
}