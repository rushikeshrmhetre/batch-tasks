public class AllMailingStateFromContact implements Database.Batchable<SObject>
 {
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator('SELECT AccountId, MailingState FROM Contact WHERE MailingState != null');
    }
    public void execute(Database.BatchableContext bc, List<Contact> lstContact)
    {  // set<Id> setIds = new List<Id>();
        Map<Id, String> mapAccIdMailState = new Map<Id, String>();
        for (Contact objCon : lstContact)
        {
            if(!mapAccIdMailState.containsKey(objCon.AccountId))
            {
                mapAccIdMailState.put(objCon.AccountId, objCon.MailingState);
            }
            else 
            {   
                boolean boolcheckstate = mapAccIdMailState.get(objCon.AccountId).contains(objCon.MailingState);
                if(!boolcheckstate)
                {
                    String strState = mapAccIdMailState.get(objCon.AccountId) + ', ' + objCon.MailingState;
                    mapAccIdMailState.put(objCon.AccountId, strState);
                }    
            }
        }
        List<Account> lstAcc = [SELECT Id, (SELECT Id, MailingState FROM Contacts) FROM Account];

        for(Account objAcc : lstAcc)
        {
            if(objAcc.Contacts.size() == 0)   
            {
                mapAccIdMailState.put(objAcc.Id, NULL);  
            }
        }

        List<Account> lstAccounts = new List<Account>();
        for(Id accId : mapAccIdMailState.keySet())
        {
            Account objAcc = new Account();
            objAcc.Id = accId;
            objAcc.All_Mailing_States__c = mapAccIdMailState.get(accId);
            lstAccounts.add(objAcc);
        }

        if(!lstAccounts.isEmpty())
        {
            update lstAccounts;
        }

    }

    public void finish(Database.BatchableContext bc)
    {
        System.debug('Done');
    }
}
//check the update changes...