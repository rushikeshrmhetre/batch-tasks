global class ScheduleBatchForMailingStateFromCons implements Schedulable
{
    public void execute(SchedulableContext objSC) 
    {
        AllMailingStateFromContact objMsFromCon = new AllMailingStateFromContact();
        database.executebatch(objMsFromCon);
    }
}