@isTest
public class TestAllMailStateOnAccz
{
    @testSetup 
    static void setupMethod()
    {
        List<Account> lstAcc = new List<Account>();

        Account objAccount = new Account(Name = 'Test Divyansh');
        lstAcc.add(objAccount);
        
         Account objAccount2 = new Account(Name = 'Test Divyansh2');
         lstAcc.add(objAccount2);
        
         insert lstAcc;

         List<Contact> lstCons = new List<Contact>();

        Contact objContact = new Contact(lastName = 'Test Divyansh Contact', AccountId = objAccount.Id, MailingState = 'MH');
        lstCons.add(objContact);
        
        Contact objContactSec = new Contact(lastName = 'Test Divyansh Contact', AccountId = objAccount.Id, MailingState = 'KL');
        lstCons.add(objContactSec);
        
        Contact objContact3 = new Contact(lastName = 'Test Divyansh Contact', AccountId = objAccount.Id, MailingState = 'DL');
        lstCons.add(objContact3);
        
        insert lstCons;

       
        
    }
    
    @isTest
    public static void testMethod1()
    {
        AllMailingStateFromContact objMS = new AllMailingStateFromContact();
        Id batchId = Database.executeBatch(objMS);
        test.startTest();
        Account objAccount = [Select Id, All_Mailing_States__c From Account LIMIT 1];
        System.assertEquals('MH, DL', objAccount.All_Mailing_States__c);
        test.stopTest();
 
    }
    
    @isTest 
    public static void deleteMethod()
    {
        Id accId2= [SELECT Id,Name FROM Account WHERE Name = 'Test Divyansh2'].Id;
        Contact objContact4 = new Contact(lastName = 'Test Divyansh Contact4', AccountId = accId2, MailingState = 'DL');
        insert objContact4;
       
        List<Contact> lstConDel = [SELECT Id, MailingState FROM Contact WHERE AccountId =: accId2];
        
        
        test.startTest();
        AllMailingStateFromContact objMS = new AllMailingStateFromContact();
        Id batchId = Database.executeBatch(objMS);
        test.stopTest();
        
        Account objAccount = [Select Id, All_Mailing_States__c From Account WHERE Name =:'Test Divyansh2'  LIMIT 1];
        System.assertEquals(NULL, objAccount.All_Mailing_States__c);

	}
  
}